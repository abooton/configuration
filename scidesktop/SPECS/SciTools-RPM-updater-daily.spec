Name:           SciTools-RPM-updater-daily
Version:        1.0
Release:        4
Summary:        A cron job to run /opt/scitools/.updater/bin/update-scitools.sh daily

License:        BSD 3
Group:          Unspecified

BuildRoot:      %{_tmppath}/SciTools-RPM-updater-daily

Requires:   SciTools-RPM-updater


%description

Runs the update-scitools.sh script daily

%prep
# Clear up any pre-existing build-root.
rm -rf ${RPM_BUILD_ROOT}/
mkdir -p ${RPM_BUILD_ROOT}

%install
export CRON_DIR=/etc/cron.d
export CRONFILE=${CRON_DIR}/scitools-update.cron
mkdir -p ${RPM_BUILD_ROOT}${CRON_DIR}

cat <<EOF > ${RPM_BUILD_ROOT}${CRONFILE}
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

55 6 * * 1-5 root /opt/scitools/.updater/bin/update-scitools.sh >> /var/log/scitools_cron.log 2>&1

EOF

%files
%attr(0644, root, root) "/etc/cron.d/scitools-update.cron"

%changelog
* Thu Apr 04 2019 Bill Little <bill.little@metoffice.gov.uk> - 1.0-4
- Add standard root cron preamble, see /etc/cron.d scripts

* Wed Apr 03 2019 Bill Little <bill.little@metoffice.gov.uk> - 1.0-3
- Remove crontab entry root user field

* Wed Aug 01 2018 Bill Little <bill.little@metoffice.gov.uk> - 1.0-2
- Append to log for log rotation support

* Wed Aug 01 2018 Philip Elson <philip.elson@metoffice.gov.uk> - 1.0-1
- Initial RPM release

