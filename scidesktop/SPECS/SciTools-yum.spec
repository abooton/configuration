Name:           SciTools-yum
Version:        4
Release:        1
Summary:        Enable the http://exvscitoolsrepoprd/rhel6/RPMS repo

License:        BSD 3
Group:          Unspecified

BuildRoot:      %{_tmppath}/SciTools-yum


%description

Enable the http://exvscitoolsrepoprd/rhel6/RPMS repo

%prep
# Clear up any pre-existing build-root.
rm -rf ${RPM_BUILD_ROOT}/
mkdir -p ${RPM_BUILD_ROOT}

%install
mkdir -p ${RPM_BUILD_ROOT}/etc/yum.repos.d
touch ${RPM_BUILD_ROOT}/etc/yum.repos.d/mo-scitools.repo
mkdir -p ${RPM_BUILD_ROOT}/etc/pki/rpm-gpg
touch ${RPM_BUILD_ROOT}/etc/pki/rpm-gpg/RPM-GPG-KEY-AVD

%files
%attr(0644, root, root) "/etc/yum.repos.d/mo-scitools.repo"
%attr(0644, root, root) "/etc/pki/rpm-gpg/RPM-GPG-KEY-AVD"

%post -p /bin/sh
  if [ $1 = 2 ]; then
    # Do stuff specific to upgrades
    rm /etc/yum.repos.d/mo-scitools.repo
    rm -rf /etc/pki/rpm-gpg/RPM-GPG-KEY-AVD
  fi
  echo "Adding an MO-SCITOOLS repo";

  echo "[SciTools]" > /etc/yum.repos.d/mo-scitools.repo
  echo "name = SciTools environments" >> /etc/yum.repos.d/mo-scitools.repo
  echo "baseurl = http://exvscitoolsrepoprd/rhel6/RPMS/" >> /etc/yum.repos.d/mo-scitools.repo
  echo "enabled = 1" >> /etc/yum.repos.d/mo-scitools.repo
  echo "gpgcheck = 1" >> /etc/yum.repos.d/mo-scitools.repo
  echo "gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-AVD" >> /etc/yum.repos.d/mo-scitools.repo
  echo "-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.14 (GNU/Linux)

mQENBFerA6IBCACtrhKsw+njcCn1ObczQH/uu2IthEfa0nmDUjVuGxKJn70P43h5
QJgIflL4sRweolK3hz8sM7ccm5LUSMbn7abjd8FO9aaVoZBlhBD53K1ysog20wnU
cNN4PeJoCBg8uyiwDS6Y51DpNA0/lwE+nUoO97MAFeYEL0u9zl5+YcJ1gXT6NMv2
19jlNDva9kmWz8t5fJdNdgty2m1BAF/eP95t7EXe2WA4bmz+jes/nIgyH8lP+Bjo
uzYGtIcqOqwunRmL3AxodLoqB1op3rlv5szRmJs33Y5Wm5Z7V/N4NGaksOgJSDYu
IM914IkBY+573f606dsyvDaZF4SP6bgPPGoHABEBAAG0RE1PIEFWRCBUZWFtIChS
UE0gc2lnbmluZyBpZGVudGl0eSkgPG1sLWF2ZC1zdXBwb3J0QG1ldG9mZmljZS5n
b3YudWs+iQE4BBMBAgAiBQJXqwOiAhsDBgsJCAcDAgYVCAIJCgsEFgIDAQIeAQIX
gAAKCRBjKWX2nX5IQV4PB/99Na8JhvJKMdAgsE2oCy400CPebnXlUlWw3/OxfnPR
6gERVIFlrnk7ZM+dZb0/nS3KHbTYuVk8Etmm43a5ur6Dn6z0KNh35oYi82hX1fmG
V/9wPykj58d51PKZyeHhaVKBqzIIGqbnSEP55GBG6afTxk3KMHCSE+pUNvhALA0b
cSjW/NkNkREr/Om3fsDuN1tlpgA4dTlodV1go4/MKOcJoUxKGtrTgWt1fykSptIs
o7Be9hUz4g+6rhYEJYAW5hSRrIv+crH9UqjFptSOt6iWO3jVK8AGCfYefxIW5ZNd
UXk5nMH7W408STraVnUzrvG3mO1TJZ1UjMY237B7G/n3uQENBFerA6IBCADxcjT2
0BstR58hj9sO/8JvWJsDJlgnGrDzI9/36cPR2ALyPxhSAaebA/D3dV6Pq2pdq/Oj
ZZ2g02Rzhz4yu6IU5mOt/Md0YvAa/KIRKzQvuLxZU7NQ+ZgzrqvPbYqlJR0sUMyv
B1PL61LxFujzQvGqm+jCpRop75usyD44//B+fEpRON1Zm7eR11CVpk8sZvMT4J7m
BAtqsVQXdzxdJGFRe6Nel1KmA9R6xl3rp5Yiq12COBNxVi2zlUPjfTut+p39SD+7
EesNO4STi3iIpynLgKgFZtTRt8Brtp6+OZspmkH+uaTo4ddwOgC7NlDTLurGDFcI
r5ORv87zUHNYxE3TABEBAAGJAR8EGAECAAkFAlerA6ICGwwACgkQYyll9p1+SEGx
Rgf+MDZK5UfwqY4X2DzCWeTRTrxxHPRKRZsuj8ZwQc+8Et0efv+UChXh6d7PDk2b
vdSiBMue5IkUIadlxTLvCiVszRcHYNwkzVRvWFRc07YoE3Ky7WA7VFh7iYaEJjMy
z3zlgjuMalfZ1zvxIYpBdWaAS6e/j4xyYAjxFaMyQ1GoaNsAfHqvsCSQywHMS87w
dsNvVR9gy2/ETHSI7vBNraljgnW2MxY8isCVXHY3D04ApvIpEs4uzjzg7Q6nrJxF
ezPv4zGiEexgwV7uRgifGbAwlXPz+Z+FVx2YRu/687S+vPIKM+Yn+0WZ8CPTIrDa
EPSwj5IhPJAJgVFWCtmHhd/6Gw==
=lNzL
-----END PGP PUBLIC KEY BLOCK-----" > /etc/pki/rpm-gpg/RPM-GPG-KEY-AVD
  rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-AVD

# Run *after* the RPM is upgraded or uninstalled (https://wiki.mageia.org/en/Packagers_RPM_tutorial#Pre-_and_Post-installation_scripts).
%postun -p /bin/sh
  if [ $1 = 0 ]; then
      # Do stuff specific to uninstalls
      rm -rf /etc/yum.repos.d/mo-scitools.repo
      rm -rf /etc/pki/rpm-gpg/RPM-GPG-KEY-AVD
  fi


