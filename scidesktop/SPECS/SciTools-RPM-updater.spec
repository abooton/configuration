Name:           SciTools-RPM-updater
Version:        1.0
Release:        7
Summary:        An "update-scitools" script to yum update scitools and auto-remove obsoletes

License:        BSD 3
Group:          Unspecified

BuildRoot:      %{_tmppath}/SciTools-RPM-updater

# Needs package-cleanup from yum-utils.
Requires: yum-utils
Requires: SciTools-logrotate


%description

Install the root-owned update-scitools.sh script in /opt/scitools/.updater/bin/

%prep
# Clear up any pre-existing build-root.
rm -rf ${RPM_BUILD_ROOT}/
mkdir -p ${RPM_BUILD_ROOT}

%install
export BIN_DIR=/opt/scitools/.updater/bin
mkdir -p ${RPM_BUILD_ROOT}/${BIN_DIR}
export UPDATE_SCITOOLS=${RPM_BUILD_ROOT}/${BIN_DIR}/update-scitools.sh

cat <<'EOF' > ${UPDATE_SCITOOLS}
#!/bin/sh

LOGFILE=/var/log/scitools_updater.log

# Add preamble entry to log.
DIVIDER="################################################################################"
echo -e "\n${DIVIDER}\n$(date)\n\n" >> ${LOGFILE}

# Update all SciTools RPMs from:
#   - the [SciTools] repo for RHEL6, see /etc/yum.repos.d/mo-scitools.repo
#   - the [scitools] repo for RHEL7, see /etc/yum.repos.d/scitools.repo
yum -y --disablerepo="*" --enablerepo="SciTools,scitools" update 2>&1 | tee -a ${LOGFILE}

OBSOLETE_PKGS=/var/log/scitools_obsolete_packages

# Identify RPMs that are now obsolete.
# See also: https://unix.stackexchange.com/a/56056/156176
package-cleanup --leaves -q --all \
    | grep ^SciTools \
    | xargs repoquery --installed --qf '%{nvra} - %{yumdb_info.reason}' 2>>${LOGFILE} \
    | grep -- '- dep' \
    | cut -d' ' -f1  > ${OBSOLETE_PKGS}

if [ -s "${OBSOLETE_PKGS}" ]
then 
   echo "The following RPMs will be removed:" 2>&1 | tee -a ${LOGFILE}
   cat ${OBSOLETE_PKGS} | tee -a ${LOGFILE}
   xargs yum remove -y < ${OBSOLETE_PKGS} 2>&1 | tee -a ${LOGFILE}
else
   echo "No SciTools RPMs are awaiting removal." 2>&1 | tee -a ${LOGFILE}
fi

[ -f "${OBSOLETE_PKGS}" ] && rm -f ${OBSOLETE_PKGS}

EOF

%files
%attr(0744, root, root) "/opt/scitools/.updater/bin/update-scitools.sh"

%changelog
* Wed Feb 27 2019 <bill.little@metoffice.gov.uk> - 1.0-7
- Add logrotate

* Fri Feb 15 2019 Bill Little <bill.little@metoffice.gov.uk> - 1.0-6
- Support [scitools] repo rename for RHEL7

* Wed Aug 08 2018 Bill Little <bill.little@metoffice.gov.uk> - 1.0-5
- fix log output

* Wed Aug 08 2018 Bill Little <bill.little@metoffice.gov.uk> - 1.0-4
- Added yum -y flag to update

* Tue Aug 07 2018 Bill Little <bill.little@metoffice.gov.uk> - 1.0-3
- Add log preamble and capture repoquery stderr to log

* Wed Aug 01 2018 Bill Little <bill.little@metoffice.gov.uk> - 1.0-2
- Append to log for log rotation support and perms change

* Wed Aug 01 2018 Philip Elson <philip.elson@metoffice.gov.uk> - 1.0-1
- Initial RPM release

