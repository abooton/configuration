#!/usr/bin/env bash

#
# Purpose:
#   Refresh all the builds for the 'scidesktop' platform.
# Details:
#   Run a conda-build-all for :
#       ssh://git@exxgitrepo:7999/sss/core-linux-conda-recipes.git
#   Update the channel at /project/avd/live/conda/RedHat.x86_64/linux-64
#
# Note: the build will take place without updating the channel unless "$CHANNEL_UPDATE == true".
# Any newly built results can be found in ${LOCALDATA}/conda-building/new_distributions.
#

# conda execute
# env:
#  - cmake
#  - conda 4.3.*
#  - conda-build-all
#  - conda-build <3
#  - git
#  - jinja2
#  - requests <2.19
# channels:
#  - conda-forge
#  - defaults
# run_with: bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Get the directory of this script.
export SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

source ${SCRIPT_DIR}/env_setup.sh

# The location of the checked out recipes.
RECIPES_DIR=${WORKDIR}/recipes

# In order for this git to use Met Office self-signed certificates, we need to enable them.
# We should re-build certifi with the Met Office certs to avoid this.
# Worse, some systems (looking at you els machines), have extremely old ca-certificate RPMs, that pre-date the ENV_REPO,
# so we have copied the certificates into this repo from /etc/pki/tls/cert.pem.
cp ${SCRIPT_DIR}/../../shared/make_RPMs/cert.pem ${PREFIX}/ssl/cacert.pem

# Delete ${RECIPES_DIR} if it exists, and recreate it from fresh.
[ -d ${RECIPES_DIR} ] && rm -rf ${RECIPES_DIR}
mkdir -p ${RECIPES_DIR}
git clone --quiet https://exxgitrepo:8443/scm/sss/core-linux-conda-recipes.git ${RECIPES_DIR}

# Create the channel root, and ensure it is an actual conda channel.
mkdir -p ${PLATFORM_ROOT}
conda index ${PLATFORM_ROOT}

# Create a fresh directory for the conda-build root.
[ -d ${CONDA_BUILD_ROOT} ] && rm -rf ${CONDA_BUILD_ROOT}
mkdir -p ${CONDA_BUILD_ROOT}

# To save us having to download it each time, save the caches that we care
# about into the parent directory (which is not purged) and link to it from
# the build root directory (which is purged).
mkdir -p ${WORKDIR}/src_cache
ln -s ${WORKDIR}/src_cache ${CONDA_BUILD_ROOT}/
mkdir -p ${WORKDIR}/git_cache
ln -s ${WORKDIR}/git_cache ${CONDA_BUILD_ROOT}/

${SCRIPT_DIR}/build.sh ${RECIPES_DIR}

if [ "${CHANNEL_UPDATE}" == "true" ]; then
    # Move any new builds to the PLATFORM_ROOT and make them available.
    if [ -n "$(ls -A ${NEW_DISTRIBUTIONS_DIR})" ]; then
       mv ${NEW_DISTRIBUTIONS_DIR}/* ${PLATFORM_ROOT}
       conda index ${PLATFORM_ROOT}
       echo "Updated"
       exit 0
    fi
fi

echo "Unchanged"
