# Scientific Software Stack Configuration

This repository contains tools used to manage the building of conda recipes, internal conda channels, RPM building and environment deployments.

This supports the provision of Scientific Software Environments onto the Scientific Desktop Estate and onto the Cray HPC.


Below are links to information about:
* [Scientific Software Stack Documentation](readthedocs/README.md)

