- must be run on els056 as it has an installation of /usr/bin/rpmsign
- test with your own CDN user (no need to be avd)

- create _conda-rpms environment with:
    - conda create -n _conda-gitenv -c conda-forge conda=4.3 'conda-build<3.18.3' conda-build-all conda-gitenv git python=2
    - conda create -n _conda-rpms -c conda-forge conda=4.1.2 conda-build=2.0.6 conda-build-all=1.1.0 conda-gitenv=0.2.2 pip python=2
    - pip install -e .  (for conda-rpms dev git branch)

- run with the _conda-rpms environment

- mv ${CONDA_PREFIX}/ssl/cacert.pem ${CONDA_PREFIX}/ssl/cacert.pem.conda
- cp ./cert.pem ${CONDA_PREFIX}/ssl/cacert.pem.mo
- ln -sf ${CONDA_PREFIX}/ssl/cacert.pem.conda ${CONDA_PREFIX}/ssl/cacert.pem

