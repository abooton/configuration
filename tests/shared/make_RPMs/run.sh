#!/usr/bin/env bash

PROG=$(basename ${0} .sh)


oops() {
    local EMSG="${1}"

    echo -e "\n${PROG}: ${EMSG}\n"

    exit 1
}


################################################################################
[ $(hostname) != "els056" ] && oops "Test on els056 only!"

BASE=${LOCALTEMP}/rpms

SPECS_LOCATION=${BASE}/rpmbuild_root
[ -d ${SPECS_LOCATION} ] && rm -rf ${SPECS_LOCATION}
mkdir -p ${SPECS_LOCATION}/SPECS
BUILT_RPMS_DIR=${SPECS_LOCATION}/RPMS/x86_64
mkdir -p ${BUILT_RPMS_DIR}

PACKAGE="SciTools"
CONFIG_YAML=${BASE}/conda_rpms_config.yaml

cat <<-EOF >${CONFIG_YAML}
rpm:
    prefix: ${PACKAGE}

install:
    prefix: /opt/scitools

module:
    prefix: /opt/scitools/modulefiles
    file: ${BASE}/modulefile.agnostic
    default: ${BASE}/modulefile.default
EOF

REPO_DIR=${BASE}/environments
API_USER="<artifactory-user>@metoffice.gov.uk"
API_KEY="<artifactory-user-key>"

export CONDARC="${BASE}/${PROG}_condarc"
mkdir -p $(dirname ${CONDARC})
cat <<EOF >${CONDARC}

add_pip_as_python_dependency: False
ssl_verify: False

EOF

[ -z "${CONDA_PREFIX}" ] && oops 'Not running within a conda environment, missing "${CONDA_PREFIX}".'
[ ! -f "${CONDA_PREFIX}/ssl/cacert.pem.conda" ] && oops "Missing ${CONDA_PREFIX}/ssl/cacert.pem.conda."
[ ! -f "${CONDA_PREFIX}/ssl/cacert.pem.mo" ] && oops "Missing ${CONDA_PREFIX}/ssl/cacert.pem.mo."
[ ! -h "${CONDA_PREFIX}/ssl/cacert.pem" ] && oops "Missing symbolic-link ${CONDA_PREFIX}/ssl/cacert.pem."

ln -sf ${CONDA_PREFIX}/ssl/cacert.pem.conda ${CONDA_PREFIX}/ssl/cacert.pem

################################################################################
echo -e "\n${PROG}: build rpm structure...\n"

python -m conda_rpms.build_rpm_structure ${REPO_DIR} ${SPECS_LOCATION} -c ${CONFIG_YAML} -u ${API_USER} -k ${API_KEY} --env_labels experimental/*
python -m conda_rpms.build_rpm_structure ${REPO_DIR} ${SPECS_LOCATION} -c ${CONFIG_YAML} -u ${API_USER} -k ${API_KEY} --env_labels default/*

################################################################################
echo -e "\n${PROG}: build...\n"

python -m conda_rpms.build ${SPECS_LOCATION} ${BUILT_RPMS_DIR}

################################################################################
echo -e "\n${PROG}: sign built rpms...\n"

ln -sf ${CONDA_PREFIX}/ssl/cacert.pem.mo ${CONDA_PREFIX}/ssl/cacert.pem

for FNAME in ${BUILT_RPMS_DIR}/*.rpm
do
    # Ensure user and group read-write permissions and other read-only permissions.
    chmod -R 0664 ${FNAME}

    # Sign the RPMs with the AVD GPG key.
    # Remove carriage returns from the output.
    ${BASE}/sign_rpm.exp ${FNAME} | sed 's/\r//'
done

################################################################################
echo -e "\n${PROG}: copy built rpms to channel...\n"

REMOTE_USER="<ipa-user>"
REMOTE_RPMS_DIR="/var/www/html/${REMOTE_USER}/RPMS"
REMOTE_HOSTNAME="exvscitoolsrepoprd"

# Configure the remote RPM repository.
ssh -o BatchMode=yes ${REMOTE_USER}@${REMOTE_HOSTNAME} "[ ! -d ${REMOTE_RPMS_DIR} ] && mkdir -p ${REMOTE_RPMS_DIR}"

# Copy the built RPMs into the remote RPM repository.
CHANGES=$(rsync -icrlpoD --ignore-existing ${BUILT_RPMS_DIR} ${REMOTE_USER}@${REMOTE_HOSTNAME}:${REMOTE_RPMS_DIR})

################################################################################
echo -e "\n${PROG}: copy built rpms to channel...\n"

if [ -n "${CHANGES}" ]
then
    # Create/update the RPM repository.
    ssh -o BatchMode=yes ${REMOTE_USER}@${REMOTE_HOSTNAME} createrepo -v --update --database ${REMOTE_RPMS_DIR}

    # Make the RPM repository browseable.
    ssh -o BatchMode=yes ${REMOTE_USER}@${REMOTE_HOSTNAME} repoview ${REMOTE_RPMS_DIR}

    echo "Updated"
else
    echo "Unchanged"
fi
