#%Module1.0
#
# scitools - scientific software stack for the '{{ env.name }}/{{ env.label }}' environment
#

set ENV_NAME "{{ env.name }}-{{ env.label }}"
set LINK_DIR "{{ env.prefix }}/environments/{{ env.name }}/{{ env.label }}"
set BASE_DIR [exec readlink -f $LINK_DIR ]
set HELP "Scientific software stack for the '{{ env.name }}/{{ env.label }}' environment."

set ENV_MODULE_DIR [glob "$BASE_DIR/cray/*.mod"]
set ALL_MODULE_DEPS ""
set LOADED_MODULE_DEPS ""

# When this module is unloaded, unload the module dependencies that were 
# loaded.
if {[info exists env(SCITOOLS_LOADED_MODULE_DEPS)]} {
    set LOADED_MODULE_DEPS $env(SCITOOLS_LOADED_MODULE_DEPS)
    foreach MODULE_DEP $LOADED_MODULE_DEPS {
        module unload $MODULE_DEP
    }
}

# Populate ALL_MODULE_DEPS, a list of the unique modules that are required to 
# be loaded for the environment to work
foreach ENV_MODULE_FILENAME $ENV_MODULE_DIR {
    set FILEHANDLE [open $ENV_MODULE_FILENAME "r"]
    set MODULE_DEPS [split [string trim [read $FILEHANDLE]]]
    foreach MODULE_DEP $MODULE_DEPS {
        if {$MODULE_DEP ni $ALL_MODULE_DEPS} {
            lappend ALL_MODULE_DEPS $MODULE_DEP
        }
    }
    close $FILEHANDLE
}

proc ModulesHelp { } {
    global ENV_NAME
    global LINK_DIR
    global BASE_DIR
    global HELP
    global ALL_MODULE_DEPS

    puts stderr $HELP
    puts stderr ""
    puts stderr "This environment is available via:"
    puts stderr "   - the symbolic link $LINK_DIR, or"
    puts stderr "   - the base directory $BASE_DIR"
    puts stderr ""
    puts stderr "Note that, the environment symbolic link points to the environment base"
    puts stderr "directory."
    puts stderr ""
    puts stderr "As new software is deployed to the scientific software stack, this environment"
    puts stderr "symbolic link is automatically updated to point to the appropriate environment"
    puts stderr "base directory."
    puts stderr ""
    puts stderr "For this environment to work, the following modules are automatically loaded:"
    puts stderr "   - $ALL_MODULE_DEPS"
    puts stderr ""
    puts stderr "For documentation, see http://www-avd/sci/software_stack/"
}

module-whatis $HELP

# Load each module dependecy if it is not already loaded.
# If a module is loaded here, capture it in the LOADED_MODULE_DEPS variable so
# that it can be unloaded at module unload time.
foreach MODULE_DEP $ALL_MODULE_DEPS {
    if { ![is-loaded $MODULE_DEP] } {
        module load $MODULE_DEP
        lappend LOADED_MODULE_DEPS $MODULE_DEP
    }
}

conflict scitools um_tools

prepend-path PATH $BASE_DIR/bin

setenv SCITOOLS_LOADED_MODULE_DEPS $LOADED_MODULE_DEPS
setenv SSS_ENV_DIR $LINK_DIR
setenv SSS_ENV_NAME $ENV_NAME
setenv SSS_TAG_DIR $BASE_DIR
