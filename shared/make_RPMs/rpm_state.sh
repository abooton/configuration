#!/usr/bin/env bash


PROG=$(basename ${0})
NARGS=2

if [ ${#} -ne ${NARGS} ]
then
    echo "${PROG}: Expected ${NARGS} arguments, got ${#}."
    exit 1
fi

PACKAGE="${1}"
BASEDIR="${2}"
RPM_LABEL_GLOB="${BASEDIR}/x86_64/${PACKAGE}-env-*-label-*"
CURRENT_BRANCH=
CURRENT_LABEL=

ls -1 ${RPM_LABEL_GLOB} | sort |
while read FNAME
do
    BASENAME=$(basename ${FNAME})
    BRANCH=$(cut -d'-' -f3 <<< ${BASENAME})
    COUNT=$(wc -w <<< $(tr "-" " " <<< ${BASENAME}))
    if [ ${COUNT} -eq 7 ]
    then
        LABEL=$(cut -d'-' -f5 <<< ${BASENAME})
    else
        LABEL=$(cut -d'-' -f5,6 <<< ${BASENAME})
    fi
    REQUIRES=$(rpm -qRp ${FNAME} 2>/dev/null | grep ${PACKAGE})
    TAG=$(cut -d'-' -f2,3,5- <<< ${REQUIRES})
    echo "${BRANCH},${LABEL},${TAG}"
done | sort -u |
while read SPEC
do
    BRANCH=$(cut -d',' -f1 <<< ${SPEC})
    LABEL=$(cut -d',' -f2 <<< ${SPEC})
    TAG=$(cut -d',' -f3 <<< ${SPEC})
    if [ "${CURRENT_BRANCH}" != "${BRANCH}" ]
    then
        echo "${BRANCH}:"
        CURRENT_BRANCH="${BRANCH}"
        CURRENT_LABEL=
    fi
    if [ "${CURRENT_LABEL}" != "${LABEL}" ]
    then
        echo "    ${LABEL}:"
        CURRENT_LABEL="${LABEL}"
    fi
    echo "        - ${TAG}"
done
