.. include:: ../globals.rst.inc

:orphan:


.. raw:: html

    <head>
        <title>{{ text_title }}</title>

        <style>
            <!-- css used by the python difflib -->
            table.diff {font-family:Courier; border: small;}
            .diff_header {background-color:#e0e0e0}
            td.diff_header {text-align:right}
            .diff_next {background-color:#c0c0c0}
            .diff_add {background-color:#aaffaa}
            .diff_chg {background-color:#ffff77}
            .diff_sub {background-color:#ffaaaa}
        </style>

        <script>
            function openChannelDiff() {
                let params = ``;
                var win = window.open("", "", params);
                win.document.body.innerHTML = '{{ text_diff_packages_compressed }}';
            }
        </script>
    </head>


Comparing Environments
======================

.. include:: autogen-sss-env-list-diff-form.rst.inc


{{ text_title }}
---------------------------------------------------------------------------------------------------

This difference only shows the Python packages in each environment
:term:`manifest`.  This excludes the :term:`conda channel`  for easier
comparison.

The difference report was created using the standard Python
`difflib <https://docs.python.org/3/library/difflib.html#difflib.HtmlDiff>`_
class.

.. note:: If you want to see the full difference including the conda
          channel click the button below.  This will create a new window to
          cater for more content.

.. raw:: html

    <!-- Show a button and the diff html inline -->
    <button id="openDiffWindowButton" onclick="openChannelDiff()">View full diff including conda channels</button>
    <p></p>

    <div id="packages">
        <font face="courier" size="-1">
            {{ text_diff_packages_only }}
        </font>
    </div>

    <p></p>

    <!-- Standard legend html that the difflib.html produces -->
    <table class="diff" summary="Legends">
        <tr> <th colspan="2"> Legends </th> </tr>
        <tr> <td> <table border="" summary="Colors">
                      <tr><th> Colors </th> </tr>
                      <tr><td class="diff_add">&nbsp;Added&nbsp;</td></tr>
                      <tr><td class="diff_chg">Changed</td> </tr>
                      <tr><td class="diff_sub">Deleted</td> </tr>
                  </table></td>
             <td> <table border="" summary="Links">
                      <tr><th colspan="2"> Links </th> </tr>
                      <tr><td>(f)irst change</td> </tr>
                      <tr><td>(n)ext change</td> </tr>
                      <tr><td>(t)op</td> </tr>
                  </table></td> </tr>
    </table>

    <p></p>
