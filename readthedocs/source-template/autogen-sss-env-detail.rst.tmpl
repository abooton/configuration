.. include:: ../globals.rst.inc
:orphan:



Environment: {{ sss_env_info[0]["desc"] }}-{{ sss_env_info[0]["label"] }}
-------------------------------------------------------------------------------


Environment Info
^^^^^^^^^^^^^^^^

{#####List of envs #####}

.. list-table::

  * - **Desc-Tag**
    - **Released**
    - **Python Version**
    - **Core Packages**
{% for env in sss_env_info %}
  * - `{{ env["desc"] }}-{{ env["label"] }} <autogen-sss-env-detail-{{ env["unique_name"] }}.html>`_
    - {{ env["released"] }}
    - `{{ env["python_version"] }} <autogen-sss-package-detail-{{ env["python_version_full"] }}.html>`__
    - {% for package in env["core_packages"] %} `{{ package[0] }} <autogen-sss-package-detail-{{ package[1] }}.html>`_ |br|
      {%- endfor %}
{% endfor %}



.. note:: The environment :term:`manifest` for this environment is held in
          Bitbucket `{{ sss_env_info[0]["env_tag"] }} <{{ sss_env_info[0]["manifest_url_web"] }}>`_
          and is used to create this documentation page.


{# Header info including a list of channels used in this env #}


Packages
^^^^^^^^

There are a total of **{{ total_packages }} package(s)**  in a
total of **{{ sss_env_info[0]['packages']|length  }}** :term:`conda channel` (s) used
in this environment.


{% for channel in sss_env_info[0]['packages'] %}
* {{ channel }} (`repodata.json <{{ channel }}/repodata.json>`__)
{% endfor %}

.. note:: As of Oct 31st 2020 the internal server
          https://exxmavenrepo:8443/artifactory/ that has hosted conda channels
          used in the Scientific Software Stack, has been retired in favour of
          the Met Office cloud artifactory instance
          https://metoffice.jfrog.io/.  To ensure
          the environment browser continues to provide the package detail a
          static copy of the **repodata.json** has been taken and will be used
          to generate ths environment browser contents.


{#------ List all the packages by channel #}


{% for channel in sss_env_info[0]['packages'] %}
Channel: {{ channel }}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are **{{ sss_env_info[0]['packages'][channel]|length }}
package(s)** used from this :term:`conda channel` .



.. hlist::
   :columns: 2

{% for package in sss_env_info[0]['packages'][channel] %}
   * `{{ package }} <autogen-sss-package-detail-{{ package }}.html>`_
{% endfor %}

{% endfor %}
