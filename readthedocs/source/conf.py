# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Scientific Software Stack'
copyright = '2020, AVD'
author = 'AVD'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [ 'sphinx.ext.todo',
               'sphinx.ext.duration',
               'sphinx_copybutton',
               ]

copybutton_prompt_text = "$ "
todo_include_todos = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_logo = 'images/sss-logo-title.svg'
html_favicon = '_static/favicon.ico'
html_theme = 'sphinx_rtd_theme'
html_theme_options = {
    'display_version': True,
    'style_external_links' : True,
    'logo_only' : 'True',
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_style = 'theme_override.css'

# ensure a timestamp is present on the rendered html by using:
# html_last_updated_fmt = "%d/%m/%Y, %H:%M:%S"

# url link checker, https has issues, lets ignore them
linkcheck_ignore = [
    'https://*',
    'http://freetype.sourceforge.net/FTL.TXT'
]
