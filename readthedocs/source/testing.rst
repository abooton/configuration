.. include:: globals.rst.inc

Testing
=======

Basic Script
------------

For a basic script to set-up your environments of choice and run your Python
code see the `Best Practice <best_practice#use-a-shell-script-where-possible>`_


Testing Against Multiple Environments
-------------------------------------

A simple shell script may be used to automate testing of your project code
for all of the ``default`` environments (``previous``, ``current``, ``next``).
This approach to testing will allow you to identify issues in the upcoming
environments before it affects you on the ``default-current`` environment.

The example below uses the exit code to check the success after running
a simple Python script.  A better approach would be to also call your unit
tests to ensure success.

.. code-block:: bash
   :linenos:
   :caption: call-mypython-multiple-envs.bash

   #!/bin/bash

   ENVS_TO_TEST="scitools/default-previous \
                 scitools/default-current \
                 scitools/default-next"

   # setup your PYTHONPATH if needed
   export PYTHONPATH=~me/python/my-project:${PYTHONPATH}

   for STACK in ${ENVS_TO_TEST}
   do
           echo "Loading environment: ${STACK}..."
           module load ${STACK}
           echo "SSS_ENV_DIR = ${SSS_ENV_DIR}"
           echo "SSS_TAG_DIR = ${SSS_TAG_DIR}"

           echo "Testing environment: ${STACK}..."
           python mypython.py
           echo "Return Code = ${?}"

           echo "Unloading environment: ${STACK}"
           module unload ${STACK}
           echo
   done



.. tip:: A comprehensive shell script has been developed for `StaGE <https://exxgitrepo:8443/projects/PP/repos/stage/browse/test/test_unit_tests.sh>`_.
         This script could be used as a basis for other projects.


Automate using Tools - Internal
-------------------------------

To enable automated building, testing and deployment various tools can be
used.  For more information on tools used see
`How we manage software development <https://metoffice.sharepoint.com/sites/TechnologyCommsSite/SitePages/How-we-manage-Software-Development.aspx>`_


Automated using Tools - Github and Travis CI
--------------------------------------------

This is aimed at projects that are located on `Gitub <https://github.com>`_.
There are many guides on the internet, the official
`Build a Python Project <https://docs-staging.travis-ci.com/user/languages/python>`_
from `Travis-CI <https://travis-ci.org>`_ is a good guide.

As GitHub is used it is not Met Office only and thus the Scientific
Software Stack is not suitable in its current form.  However
`conda <https://docs.conda.io/>`_ is applicable as it is commonly used
externally (conda is also the tool used to deliver the Scientific Software
Stack)

.. note:: Scientific Software Stack conda channels and packages are available from
          the Met Office Artifactory Cloud instance, contact the
          `AVD Team <contact>`_ for further details.

A `discussion thread on Yammer <https://www.yammer.com/metoffice.gov.uk/#/threads/show?threadId=187674165198848>`_
shows examples of Travis-CI use.

The video below may also be useful in understanding how to use Travi CI.
Note that the video includes docker, this was their chosen tool to deploy,
it is entirely optional.

.. raw:: html

    <iframe width="700" height="350" src="https://www.youtube.com/embed/1PC68ufAn6U"
            frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope;
            picture-in-picture" allowfullscreen>
    </iframe>
    <p></p>
