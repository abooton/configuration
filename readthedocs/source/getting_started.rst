.. include:: globals.rst.inc

.. raw:: html

    <head>
      <link rel="stylesheet" type="text/css" href="_static/asciinema-player.css" />
      <script src="_static/asciinema-player.js"></script>
    </head>

Quick Start
===========

How do I use the Stack?
-----------------------

All Scientific Software Stack environments are available via the Linux
``module`` command.

For example, to list all the ``scitools`` modules that are available:

.. code-block:: console

   $ module avail scitools

   -------------------------- /usr/share/Modules/modulefiles ---------------------------
   scitools/default-current(default)    scitools/preproduction_legacy-os43
   scitools/default_legacy-current      scitools/preproduction-os43
   scitools/default_legacy-next         scitools/production_legacy-os42-1
   scitools/default_legacy-previous     scitools/production_legacy-os43-1
   scitools/default-next                scitools/production-os40-1
   scitools/default-previous            scitools/production-os41-1
   scitools/experimental-current        scitools/production-os42-1
   scitools/experimental_legacy-current scitools/production-os43-1

.. note:: If you used the command ``module avail`` without the ``scitools``
          argument you would be presented  with a longer list of modules.

To use a sctiools environment you must first load it.  In the example below
we load ``scitools`` which will always default to the ``scitools/default-current``
environment:

.. code-block:: console

   $ module load scitools

Or, more explicitly:

.. code-block:: bash

   $ module load scitools/default-current


.. tip:: If you are a ksh user, you may sometimes have an issue with the module
         shell function.  If you find this is the case, please add the following
         to your `.kshrc`:

         .. code-block:: bash

            if [ -f /etc/kshrc ]; then
               . /etc/kshrc
            fi

         Please be wary of editing your ``.kshrc`` as it may cause issues
         outside of the Scientific Software Stack environments.



Once an environment has been loaded you can run ``python`` and it will call the
``python`` executable from within the loaded environment.

.. tip:: In order to ensure your environment has no conflicts with settings
         in your user Linux shell environment please see our
         `best practice <best_practice>`_ guide.

Here's an interactive example of exploring these commands:

.. raw:: html

    <asciinema-player src="_static/quickstart-use.cast"
                      cols="105"
                      rows="20"
                      idle-time-limit=1
                      font-size="small"
                      loop="yes">
    </asciinema-player>
    <p></p>

How do I know which version I am using?
---------------------------------------

When the ``module load`` or ``module unload`` command is used the location
of the environment you are using is changed.

To check which modules are currently loaded you can use the following command:

.. code-block:: bash

  $ module list
  Currently Loaded Modulefiles:

   1) libraries/gcc/8.1.0         8) poppler/0.61.1
   2) R/3.6.0(default)            9) latex/texlive/20180222
   3) rstudio/1.2.1335(default)  10) texstudio/2.12.6
   4) lcms/2.9                   11) pgfortran/16.10_64
   5) openjpeg/2.3.0             12) nagfor/6.1.0_64
   6) pixman/0.34.0              13) ifort/17.0_64
   7) tiff/4.0.9                 14) scitools/default-current

As you can see the ``scitools/default-current`` happens to be loaded in this
particular example.


The Linux ``module`` shell function modifies your Linux environment in a pre-defined
manner. This modification remains active for the lifetime of your session
(this is most commonly a single Linux terminal tab). This means you can have
multiple Stack environments enabled simultaneously in different Linux terminal
sessions.

When your session ends all modifications to your Linux environment made by
``module`` will be undone. You can also use ``module`` to undo
modifications to your Linux environment. For example:

.. code-block:: bash

   $ module unload scitools/default-current

``module`` also provides the ``switch`` command line option that will
``unload`` your current loaded environment and ``load`` another in one operation.
More information is available in the Linux documentation for
`module <https://modules.readthedocs.io/en/latest>`_.

Here's an interactive example of exploring these commands:

.. raw:: html

    <asciinema-player src="_static/quickstart-which-version.cast "
                      cols="105"
                      rows="20"
                      idle-time-limit=1
                      font-size="small"
                      loop="yes">
    </asciinema-player>
    <p></p>

.. tip:: You can display the currently loaded scitools module on the Linux
         command prompt. The code snippet below can be added to your
         ``.bashrc``.  Note that this example shows the loaded module and your
         prompt on the next line, you can change this by removing the
         ``\n\r`` in the highlighted line.  You may already have a custom
         prompt set so it is recommended you backup your ``.bashrc`` before
         editing.

         .. code-block:: bash
            :caption: Add to you .bashrc to set your prompt.
            :linenos:
            :emphasize-lines: 7

            # Basic prompt.  This can be removed if you have a PS1 already set
            PS1='\u@\h:\w $ '

            function sss_loaded {
               # Use $LOADEDMODULES to work out if scitools is present
               SSS=$(echo ${LOADEDMODULES} | tr ":" "\n" | grep scitools)
               [ ! -z ${SSS} ] && echo -e "(${SSS})\n\r"
            }

            # Prefix the loaded scitools name
            PS1="\$(sss_loaded)${PS1}"


         .. code-block:: bash
            :caption: Example output showing the loaded module at the prompt

            $ module load scitools
            (scitools/default-current)
            $ echo hello world
            hello world
            (scitools/default-current)
            $ module unload scitools

         An alternative is to use the defined `environment variables <environments#environment-variables>`_
         ``$SSS_TAG_DIR`` or ``$SSS_ENV_DIR`` instead of parsing ``$LOADEDMODULES`` using
         ``tr`` and ``grep``.


How do I find basic information about my environment?
-----------------------------------------------------

You can do this using the ``module display`` or ``module show`` command.
This will show you a description of the environment and its purpose, the
location of the module and the absolute path of the environment.

For example:

.. todo: use module help scitools instead of dipslay says the Bill.

.. code-block:: bash

    $ module load scitools/default-current
    $ module list
    Currently Loaded Modulefiles:
      1) libraries/gcc/8.1.0         8) poppler/0.61.1
      2) R/3.6.0(default)            9) latex/texlive/20180222
      3) rstudio/1.2.1335(default)  10) texstudio/2.12.6
      4) lcms/2.9                   11) pgfortran/16.10_64
      5) openjpeg/2.3.0             12) nagfor/6.1.0_64
      6) pixman/0.34.0              13) ifort/17.0_64
      7) tiff/4.0.9                 14) scitools/default-current
    $ module display scitools/default-current
    -------------------------------------------------------------------
    /usr/share/Modules/modulefiles/scitools/default-current:

    module-whatis	 Scientific software stack for the 'default/current' environment.
    prepend-path	 PATH /net/project/ukmo/scitools/opt_scitools/environments/default/2019_02_27/bin
    setenv		 SSS_ENV_DIR /opt/scitools/environments/default/current
    setenv		 SSS_TAG_DIR /net/project/ukmo/scitools/opt_scitools/environments/default/2019_02_27
    -------------------------------------------------------------------


.. warning:: This command will show you any other modules or
             environments which have automatically been loaded in order for
             this one to work.  This is particularly prevalent on the Cray,
             and the automatic loading of dependencies will not occur should
             you invoke an environment using a **hashbang**.

Here's an interactive example of exploring these commands:

.. raw:: html

    <asciinema-player src="_static/quickstart-info.cast "
                      cols="105"
                      rows="20"
                      idle-time-limit=1
                      font-size="small"
                      loop="yes">
    </asciinema-player>
    <p></p>

How do I know which Stack environment to use?
---------------------------------------------

This is dictated by the target platform and purpose for your Python code.
For more information see the
`environment descriptions <environments#environment-descriptions>`_.
