"""
Produce a summary of the Scientific Software Stack environments based upon the
state of the bitbucket repo.

API Doc:
* https://developer.atlassian.com/bitbucket/server/docs/latest/how-tos/command-line-rest.html
* https://developer.atlassian.com/bitbucket/api/2/reference/

API Examples:
* WEB BROWSE: https://bitbucket.org/metoffice/configuration/src/master/
* API RAW   : https://api.bitbucket.org/2.0/repositories/metoffice/configuration/src/master/scidesktop/SPECS/SciTools-SciDesktop.spec

"""

import logging
import inspect
import os
import requests
import re

# This disables the ""InsecureRequestWarning" when using https to query bitbucket.
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

BITBUCKET_RAW_LABEL_URL = (
    "https://api.bitbucket.org/2.0/repositories/metoffice/{repo}/"
    "src/{query_str}/labels/{fname}"
)
BITBUCKET_RAW_URL = (
    "https://api.bitbucket.org/2.0/repositories/metoffice/{repo}/" "src/{query}/{fname}"
)

BITBUCKET_WEB_URL = "https://bitbucket.org/metoffice/{repo}/src/{query}/{fname}"

TYPE_JSON = "JSON"
TYPE_API_TEXT = "API_TEXT"
TYPE_WEB_TEXT = "WEB_TEXT"


def autolog_info(message):
    """
    Automatically log the current function details."

    :param message: Message to log
    """

    # Dump the message + the name of this function to the log.
    logging.info(" --> %s" % (message))


def autolog_debug(message):
    """
    Automatically log the current function details."

    :param message: Message to log
    """

    # Get the previous frame in the stack, otherwise it would
    # be this function.
    func = inspect.currentframe().f_back.f_code
    head, file_name = os.path.split(func.co_filename)
    # Dump the message + the name of this function to the log.
    logging.debug(
        "%s in %s:%i \n    --> %s"
        % (func.co_name, file_name, func.co_firstlineno, message)
    )


def query_bb_api(
    repo, desc, res_type, api_host_list, fname=None, full_url=None, show=False
):
    """
    Query the Bitbucket via a url and return the response in json or text format.
    """

    if full_url:
        autolog_debug("Using the url provided...")
        url = full_url
    elif res_type == TYPE_JSON:
        autolog_debug("Using TYPE_JSON...")
        url = BITBUCKET_RAW_URL.format(repo=repo, fname=fname, query=desc)
    elif res_type == TYPE_API_TEXT:
        autolog_debug("Using TYPE_API_TEXT...")
        url = BITBUCKET_RAW_LABEL_URL.format(repo=repo, fname=fname, query_str=desc)
    elif res_type == TYPE_WEB_TEXT:
        autolog_debug("Using TYPE_WEB_TEXT...")
        url = BITBUCKET_RAW_URL.format(repo=repo, fname=fname, query=desc)
    else:
        raise NotImplemented

    auth = find_credentials(api_host_list, url)

    if show:
        autolog_info("BitBucket API Call (type={}) : {}".format(res_type, url))
        autolog_info(
            "Using API credentials : user={}) key={}...{}".format(
                auth[0], auth[1][:4], auth[1][-4:]
            )
        )

    # Need verify=False to avoid SSL issue (BitBucket cert is not signed).
    resp = requests.get(url, verify=False, auth=auth)
    resp.raise_for_status()

    # TODO: the json response may be paginated.  Needs to handle it.
    if res_type == TYPE_JSON:
        result = resp.json()

        # check that that are no pages, if there are, then raise an exception
        # https://developer.atlassian.com/bitbucket/api/2/reference/meta/pagination
        if "next" in result.keys():
            print("found next, lets raised an error")
            raise ValueError("Pagination in the API response is not handled yet...")
    elif res_type == TYPE_API_TEXT or res_type == TYPE_WEB_TEXT:
        result = resp.text.strip()
    else:
        raise NotImplemented

    return result


def find_credentials(api_host_list, url):
    """
    Check the api hosts list for a match for the channel
    """

    for host_record in api_host_list:
        if host_record["host"] in url:
            api_user = host_record["user"]
            api_key = host_record["key"]
            break
    else:
        api_user = ""
        api_key = ""

    return (api_user, api_key)


def query_json_url(url_str, api_host_list):
    """
    Query a URL (API or not) and return the response in a list of dicts.
    If the url_str is Artifactory Cloud then use the credentials.

    :param url_str: URL to use
    :param api_user: Artifactory api user
    :param api_key: Artifactory api key
    """

    auth = find_credentials(api_host_list, url_str)

    if auth:
        msg = "        Using API credentials (api_user={}) (api_key={}...{})"
        autolog_debug(msg.format(auth[0], auth[1][:4], auth[1][-4:]))

        resp = requests.get(url_str, verify=False, auth=auth)
    else:
        resp = requests.get(url_str, verify=False)

    resp.raise_for_status()
    result = resp.json()

    return result


def get_envs_from_rpm_list(rpm_list, api_host_list):
    """
    Using the rpm list populate the env details.

    :param rpm_list: list of dicts
    :param env_list: list of dicts
    """

    labels = []
    total = len(rpm_list)

    autolog_info("Retrieving Environment info...")

    for idx, rpm in enumerate(rpm_list):
        # the "desc" and "label" should already be present
        autolog_info(
            "({:02d}/{:02d}) desc={:18}  label={}".format(
                idx, total, rpm["desc"], rpm["label"]
            )
        )

        # list of repos to check
        api_platform_list = ["Scidesktop-environments", "environments"]

        for api_platform in api_platform_list:
            autolog_debug("api_platform=%s" % api_platform)

            query_str = BITBUCKET_RAW_LABEL_URL.format(
                repo=api_platform, query_str=rpm["desc"], fname=""
            )

            try:
                file_dict = query_bb_api(
                    api_platform,
                    rpm["desc"],
                    TYPE_JSON,
                    api_host_list,
                    fname="labels",
                    show=False,
                )
            except requests.exceptions.HTTPError:
                autolog_info(
                    "Exception handled "
                    "(requests.exceptions.HTTPError) "
                    "{}".format(query_str)
                )
                continue

            # for each file found
            for values_dict in file_dict["values"]:
                file_name = values_dict["path"]
                head, file_name = os.path.split(file_name)
                file_name_no_ext = file_name.rsplit(".", 1)[0]

                msg = "File found = {} (noext = {})"
                autolog_debug(msg.format(file_name, file_name_no_ext))

                # if the file_name matches a tag in the rpm then populate the
                # manifest info.

                autolog_debug("%s == %s ?" % (file_name_no_ext, rpm["label"]))

                if file_name_no_ext == rpm["label"]:
                    # get the env tag of what was actually deployed
                    # (found in a text file).
                    env_tag = query_bb_api(
                        api_platform,
                        rpm["desc"],
                        TYPE_API_TEXT,
                        api_host_list,
                        fname=file_name,
                        show=False,
                    )

                    # get the release date from the env tag.
                    released = env_tag.split("-")[2].replace("_", "-")

                    # Get URLs for the label's manifest and spec files.
                    manifest_url = BITBUCKET_RAW_URL.format(
                        repo=api_platform, fname="env.manifest", query=env_tag
                    )

                    manifest_url_web = BITBUCKET_WEB_URL.format(
                        repo=api_platform, fname="env.manifest", query=env_tag
                    )

                    rpm["manifest_url"] = manifest_url
                    rpm["manifest_url_web"] = manifest_url_web
                    rpm["env_tag"] = env_tag
                    rpm["released"] = released

    return sorted(rpm_list, key=lambda k: k["desc"])


def load_env_rpm_list(api_host_list):
    """
    Return a list of the environments that are defined as RPMs.

    :return rpm_list: List of RPMs (SSS envs)
    """

    # line format expected (there are many)
    #
    # Requires: SciTools-env-default-label-next
    # Requires: SciTools-env-default-label-current
    # Requires: SciTools-env-default-label-previous

    # Look for "^Requires: SciTools-env-"

    autolog_info("Retrieving RPM list...")
    scitools_scidesktop_spec_str = query_bb_api(
        "configuration",
        "master",
        TYPE_WEB_TEXT,
        api_host_list,
        fname="scidesktop/SPECS/SciTools-SciDesktop.spec",
        show=True,
    )

    search_str = "^Requires: SciTools-env-"

    rpm_list = []

    for aline in scitools_scidesktop_spec_str.splitlines():
        m = re.match(search_str, aline)

        if m:
            start_index = len(search_str) - 1

            # need to handle the "os43-2" so cannot just split on "-".
            # Set max of 2
            values_list = aline[start_index:].split("-", 2)

            # assuming the spec file is has consistent formatting and naming
            env_to_add = {
                "desc": values_list[0],
                "label": values_list[2],
                "released": "",
                "env_tag": "",
                "manifest_url": "",
                "manifest_url_web": "",
                "packages": [],
                "unique_name": "",
                "python_version": "",
            }

            rpm_list.append(env_to_add)

    return rpm_list


if __name__ == "__main__":
    get_envs()
